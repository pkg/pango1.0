Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1999-2007, Red Hat Software
License: LGPL-2+

Files: debian/*
Copyright: Sivaraj Doddannan
 Roozbeh Pournader
 Owen Taylor
 Karl Koehler
 Havoc Pennington
 Chookij Vanatham
 Changwoo Ryu
 Abigail Brady
 2021, Red Hat, Inc.
 2021, Jordi Mas i Hernàndez <jmas@softcatala.org>
 2021, GNOME Foundation
 2021, Benjamin Otte
 2016, Chun-wei Fan
 2012, Ryan Lortie
 2012, Emmanuele Bassi
 2010, 2012, Kristian Rietveld <kris@gtk.org>
 2008, Jürg Billeter <j@bitron.ch>
 2007, Novell, Inc
 2005-2007, Imendio AB
 2005, Amit Aronovitch
 2003, Noah Levitt
 2002-2020, Matthias Clasen
 2001, Sun Microsystems
 2001, Hans Breuer
 2001, Alexander Larsson
 2001, 2002, 2006, Behdad Esfahbod
 2000-2002, Tor Lillqvist
 2000, SuSE Linux Ltd
 2000, 2005, Keith Packard
 1999-2019, Red Hat
 1999-2006, Red Hat Software
License: LGPL-2+ and LGPL-2.1+

Files: debian/tests/installed-tests
Copyright: 2013, Canonical Ltd.
License: LGPL-2+ and LGPL-2.1+

Files: debian/tests/run-with-locales
Copyright: 2017, 2018, Collabora Ltd.
 2016-2020, Simon McVittie
License: Expat

Files: docs/urlmap.js
Copyright: 2021, GNOME Foundation
License: LGPL-2.1

Files: examples/cairoshape.c
 examples/cairotwisted.c
Copyright: no-info-found
License: NTP

Files: examples/pangowin32tobmp.c
Copyright: 2001, Hans Breuer
 1999, Red Hat Software
License: LGPL-2+

Files: meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: pango/break-arabic.c
Copyright: 2006, Sharif FarsiWeb, Inc.
 2006, Red Hat Software
License: LGPL-2+

Files: pango/break-latin.c
Copyright: 2021, Jordi Mas i Hernàndez <jmas@softcatala.org>
License: LGPL-2+

Files: pango/break-thai.c
Copyright: 2003, Theppitak Karoonboonyanan <thep@linux.thai.net>
License: LGPL-2+

Files: pango/json/*
Copyright: 2021, Benjamin Otte
License: LGPL-2.1+

Files: pango/pango-bidi-type.c
 pango/pango-bidi-type.h
Copyright: 2008, Jürg Billeter <j@bitron.ch>
License: LGPL-2+

Files: pango/pango-direction.h
 pango/pango-item-private.h
Copyright: 2002, 2018, 2020, 2021, Matthias Clasen
License: LGPL-2+

Files: pango/pango-emoji-private.h
 pango/pango-emoji.c
Copyright: 2017, Google, Inc.
License: LGPL-2+

Files: pango/pango-renderer.c
 pango/pango-renderer.h
 pango/pangocairo-fc-private.h
 pango/pangocairo-fc.h
 pango/pangocairo-private.h
 pango/pangocairo-render.c
 pango/pangocairo-win32.h
 pango/pangocairo.h
 pango/pangofc-fontmap.c
Copyright: 1999-2005, 2015, 2019, 2021, Red Hat, Inc.
License: LGPL-2+

Files: pango/pango-script.c
Copyright: 2002, Red Hat Software
License: ICU or LGPL-2+

Files: pango/pango-trace-private.h
 pango/pango-trace.c
 pango/serializer.c
Copyright: 2014, 2019-2022, 2024, Red Hat, Inc
License: LGPL-2+

Files: pango/pango-version-macros.h
Copyright: 2016, Chun-wei Fan
 2012, Ryan Lortie, Matthias Clasen and Emmanuele Bassi
 1999, Red Hat Software
License: LGPL-2+

Files: pango/pangocairo-coretext.h
 pango/pangocairo-coretextfont.h
 pango/pangocairo-coretextfontmap.c
 pango/pangocoretext.c
 pango/pangocoretext.h
Copyright: 2010, Kristian Rietveld <kris@gtk.org>
 2005-2007, Imendio AB
License: LGPL-2+

Files: pango/pangocairo-coretextfont.c
 pango/pangocoretext-private.h
Copyright: 2010, Kristian Rietveld <kris@gtk.org>
 2005-2007, Imendio AB
 2000-2005, Red Hat Software
License: LGPL-2+

Files: pango/pangocoretext-fontmap.c
Copyright: 2010, Kristian Rietveld <kris@gtk.org>
 2005-2007, Imendio AB
 2000-2003, Red Hat, Inc.
License: LGPL-2+

Files: pango/pangoft2-fontmap.c
 pango/pangoft2-private.h
 pango/pangoft2-render.c
 pango/pangoft2.c
 pango/pangoft2.h
Copyright: 2000, Tor Lillqvist
 1999, 2000, 2004, Red Hat Software
License: LGPL-2+

Files: pango/pangowin32-dwrite-fontmap.cpp
Copyright: 2022, Luca Bacci
 2022, Chun-wei Fan
License: LGPL-2+

Files: pango/pangowin32-fontcache.c
Copyright: 2007, Novell, Inc.
 2000, Tor Lillqvist
 2000, Red Hat Software
License: LGPL-2+

Files: pango/pangowin32-fontmap.c
 pango/pangowin32.c
Copyright: 2007, Novell, Inc.
 2001, Alexander Larsson
 2000, Tor Lillqvist
 1999, 2000, Red Hat Software
License: LGPL-2+

Files: pango/pangowin32-private.h
 pango/pangowin32.h
Copyright: 2001, Alexander Larsson
 2000-2002, Tor Lillqvist
 1999, Red Hat Software
License: LGPL-2+

Files: pango/pangowin32-private.hpp
Copyright: 2023, Chun-wei Fan
License: LGPL-2+

Files: pango/pangoxft.h
Copyright: 2000, SuSE Linux Ltd
 1999, Red Hat Software
License: LGPL-2+

Files: tests/markup-parse.c
 tests/test-break.c
 tests/test-common.c
 tests/test-font-data.c
 tests/test-font.c
 tests/test-itemize.c
 tests/test-layout.c
 tests/test-no-fonts.c
 tests/test-shape.c
Copyright: 2014, 2019-2022, 2024, Red Hat, Inc
License: LGPL-2+

Files: tests/test-bidi.c
 tests/test-coverage.c
 tests/test-ellipsize.c
 tests/test-harfbuzz.c
 tests/testattributes.c
Copyright: 1999-2005, 2015, 2019, 2021, Red Hat, Inc.
License: LGPL-2+

Files: tests/testboundaries_ucd.c
Copyright: 2003, Noah Levitt
License: LGPL-2+

Files: tests/testcolor.c
 tests/testcontext.c
 tests/testlanguage.c
 tests/testmatrix.c
 tests/testmisc.c
 tests/testserialize.c
 tests/testtabs.c
Copyright: 2002, 2018, 2020, 2021, Matthias Clasen
License: LGPL-2+

Files: tests/testiter.c
Copyright: 2005, Red Hat, Inc
 2005, Amit Aronovitch
License: LGPL-2+

Files: tests/testrandom.c
Copyright: 2021, Benjamin Otte
License: LGPL-2+

Files: tests/testscript.c
Copyright: 2002, Red Hat Software
License: ICU or LGPL-2+

Files: tools/gen-script-for-lang.c
Copyright: 1999-2005, 2015, 2019, 2021, Red Hat, Inc.
License: LGPL-2+

Files: utils/pango-list.c
Copyright: 2018, Google
License: LGPL-2+

Files: utils/pango-segmentation.c
Copyright: 2014, 2019-2022, 2024, Red Hat, Inc
License: LGPL-2+

Files: utils/viewer-cairo.c
 utils/viewer-cairo.h
 utils/viewer-pangocairo.c
 utils/viewer-pangoft2.c
 utils/viewer-pangoxft.c
 utils/viewer-x.c
 utils/viewer-x.h
 utils/viewer.h
Copyright: 2001, Sun Microsystems
 1999, 2004, 2005, Red Hat, Inc.
License: LGPL-2+

Files: utils/viewer-main.c
Copyright: 2006, Behdad Esfahbod
 2001, Sun Microsystems
 1999, 2004, 2005, Red Hat, Inc.
License: LGPL-2+

Files: utils/viewer-render.c
 utils/viewer-render.h
Copyright: 2001, Sun Microsystems
 1999, 2004, Red Hat Software
License: LGPL-2+

Files: docs/pango_markup.md pango/pango.rc.in pango/pangocairo.rc.in pango/pangoft2.rc.in pango/pangowin32.rc.in pango/pangoxft.rc.in tests/GraphemeBreakTest.txt tests/LineBreakTest.txt tests/SentenceBreakTest.txt tests/WordBreakTest.txt utils/test-chinese.txt utils/test-gurmukhi.txt
Copyright: 1999-2019  Red Hat
 1999-2006  Red Hat Software
 2021       Red Hat, Inc.
 2005-2007  Imendio AB
 2007       Novell, Inc
 2001       Sun Microsystems
 2000       SuSE Linux Ltd
 2005       Amit Aronovitch
 2012       Emmanuele Bassi
 2008       Jürg Billeter
 2001       Hans Breuer
 2002-2020  Matthias Clasen
 2001, 2002, 2006 Behdad Esfahbod
 2016       Chun-wei Fan
 2001       Alexander Larsson
 2012       Ryan Lortie
 2003       Noah Levitt
 2000-2002  Tor Lillqvist
 2000, 2005 Keith Packard
 2010, 2012 Kristian Rietveld
 2021       GNOME Foundation
 2021       Jordi Mas i Hernàndez
 2021       Benjamin Otte
 Owen Taylor
 Abigail Brady
 Sivaraj Doddannan
 Karl Koehler
 Havoc Pennington
 Roozbeh Pournader
 Changwoo Ryu
 Chookij Vanatham
License: LGPL-2+ and LGPL-2.1+

Files: pango/emoji_presentation_scanner.c pango/emoji_presentation_scanner.rl
Copyright:
 2018 The Chromium Authors
License: Chromium-BSD-style

Files: pango/pango-break-table.h pango/pango-emoji-table.h tests/GraphemeBreakTest.txt tests/LineBreakTest.txt tests/SentenceBreakTest.txt tests/WordBreakTest.txt
Copyright: 2016-2021 Unicode, Inc.
License: Unicode
